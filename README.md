# Backend Challenge

This is a simple CLI tool providing localized look-ups of projects from the [DonorChoose]( https://data.donorschoose.org/docs/overview/) database.

## Getting started

### Building the app from source

This project uses [Go 1.12](https://golang.org/dl/).

You can use [GNU make](https://www.gnu.org/software/make/manual/make.html) to build the application.

There are three options:

| Operating System 	| Command				   |
| -----------------	| -------------------- |
| Windows        	| `make build-windows` |
| Linux        		| `make build-linux`   |
| OS X (Mac)        | `make build-osx` 	   |

An executable binary will appear in the `/.bin` folder.

### Usage

```sh
./bin/challenge [params] --googlePlacesKey <key> --donorsChooseKey <key>
```

You can list all parameters using the `-h` option.
There are only two required parameters:

* `--googlePlacesKey <key>` 
* `--donorsChooseKey <key>`

The optional parameters are:

* `--nonStrict` enables less strict zip code matching. You will get more results, so be mindful of your Google Places API request limits
* `--verbose` displays detailed logs
* `--homeDir` allows you to set the local directory for storage
* `--perPage` allows you to customize the number of projects displayed per screen
