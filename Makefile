test:
	go test ./pkg/...
clean:
	go clean
	rm -rf ./bin/challenge*
build:
	go build -o ./bin/challenge main.go
build-windows:
	GOOS=windows GOARCH=386 go build -o ./bin/challenge.exe main.go
build-linux:
	GOOS=linux GOARCH=amd64 go build -o bin/challenge-linux main.go
build-osx:
	GOOS=darwin GOARCH=amd64 go build -o bin/challenge main.go
