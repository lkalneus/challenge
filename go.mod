module bitbucket.org/lkalneus/challenge

go 1.12

require (
	bitbucket.org/lkalneus/promptui v0.0.0-20190517042919-bf2b3320d44d
	github.com/alecthomas/gometalinter v3.0.0+incompatible // indirect
	github.com/boltdb/bolt v1.3.1
	github.com/manifoldco/promptui v0.3.2 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/sirupsen/logrus v1.4.1
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.3.2
)
