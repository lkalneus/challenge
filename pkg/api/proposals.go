package api

import (
	"errors"
	"fmt"
	"net/url"

	log "github.com/sirupsen/logrus"
)

var proposalsSearchAPI = &apiConfig{
	host: "https://api.donorschoose.org",
	path: "common/json_feed.html",
}

// ProposalsSearchResponse describes search response
type ProposalsSearchResponse struct {
	SearchTerms    string     `json:"searchTerms"`
	SearchURL      string     `json:"searchURL"`
	TotalProposals string     `json:"totalProposals"`
	Index          string     `json:"index"`
	Max            string     `json:"max"`
	Proposals      []Proposal `json:"proposals"`
}

func (p *ProposalsSearchResponse) valid() bool {
	if (p == &ProposalsSearchResponse{}) {
		log.Warn("Could not get projects data. Error: Bad response")
		return false
	}
	return true
}

// ProposalsSearchRequest describes search params.
type ProposalsSearchRequest struct {
	// Zip code
	Zip string
	// Max results per request
	Max int
	// Current index
	Index int
	// NotStrict disable strict search (results won't be for wider area)
	NotStrict bool
}

func (r *ProposalsSearchRequest) valid() bool {
	// Index can't be more than 1000 - API limit
	if r.Zip == "" || r.Max == 0 || r.Index < 0 || r.Index > 1000 {
		log.Warn("Could not get projects data. Error: Bad request")
		return false
	}
	return true
}

func (r *ProposalsSearchRequest) params() url.Values {

	q := make(url.Values)

	q.Set("zip", r.Zip)
	if !r.NotStrict {
		q.Set("keywords", "\""+r.Zip+"\"")
	}
	q.Set("max", fmt.Sprintf("%d", r.Max))
	q.Set("index", fmt.Sprintf("%d", r.Index))

	return q
}

// ProposalsSearch gets projects data
func (c *Client) ProposalsSearch(r *ProposalsSearchRequest) (ProposalsSearchResponse, error) {

	if !r.valid() {
		log.Warn("Request is invalid")
		return ProposalsSearchResponse{}, errors.New(proposalsBadRequestMsg)
	}

	var response ProposalsSearchResponse

	if err := c.getJSON(proposalsSearchAPI, r, &response); err != nil {
		log.Warn("Could not get JSON", err)
		return ProposalsSearchResponse{}, errors.New(proposalsBadResponseMsg)
	}

	if !response.valid() {
		log.Warn("Response failed validation")
		return ProposalsSearchResponse{}, errors.New(proposalsBadResponseMsg)
	}

	return response, nil
}

const proposalsBadResponseMsg = "Could not get projects data. Error: Bad response"
const proposalsBadRequestMsg = "Could not get projects data. Error: Bad request"
