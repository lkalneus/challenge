package api

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"net"
	"net/http"
	"net/http/httptest"
	"net/url"
	"time"
)

// Client may be used to make requests to external APIs
type Client struct {
	httpClient *http.Client
	apiKey     string
	baseURL    string
}

// ClientOption is the type of constructor options for NewClient(...).
type ClientOption func(*Client) error

// NewClient constructs a new Client which can make requests to external API
func NewClient(apiKey, baseURL string, options ...ClientOption) (*Client, error) {
	if apiKey == "" && baseURL == "" {
		return nil, errors.New("API credentials missing")
	}
	c := Client{
		apiKey:  apiKey,
		baseURL: baseURL,
		httpClient: &http.Client{
			Transport: &http.Transport{
				MaxIdleConnsPerHost: 20,
			},
			Timeout: 5 * time.Second,
		},
	}
	return &c, nil
}

type apiConfig struct {
	host string
	path string
}

type apiRequest interface {
	params() url.Values
}

func (c *Client) get(config *apiConfig, apiReq apiRequest) (*http.Response, error) {
	host := config.host
	q := apiReq.params()
	if c.baseURL != "" {
		host = c.baseURL
	}
	if c.apiKey != "" {
		q.Set("key", c.apiKey)
		q.Set("APIKey", c.apiKey)
	}
	req, err := http.NewRequest("GET", host+"/"+config.path, nil)
	if err != nil {
		return nil, err
	}
	req.URL.RawQuery = q.Encode()
	return c.do(req)
}

func (c *Client) do(req *http.Request) (*http.Response, error) {
	client := c.httpClient
	if client == nil {
		client = http.DefaultClient
	}
	return client.Do(req)
}

func (c *Client) getJSON(config *apiConfig, apiReq apiRequest, resp interface{}) error {
	httpResp, err := c.get(config, apiReq)
	if err != nil {
		return err
	}
	if httpResp.StatusCode != 200 {
		return errors.New("Bad response from the API")
	}
	defer httpResp.Body.Close()

	return json.NewDecoder(httpResp.Body).Decode(resp)
}

func testingHTTPClient(handler http.Handler) (*http.Client, func()) {
	s := httptest.NewTLSServer(handler)

	c := &http.Client{
		Transport: &http.Transport{
			DialContext: func(_ context.Context, network, _ string) (net.Conn, error) {
				return net.Dial(network, s.Listener.Addr().String())
			},
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	return c, s.Close
}
