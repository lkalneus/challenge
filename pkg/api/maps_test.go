package api

import (
	"net/http"
	"testing"
)

const (
	placeOkResponse = `{
		"candidates": [
			{
				"geometry": {
					"location": {
						"lat": 38.4281915,
						"lng": -121.4489256
					},
					"viewport": {
						"northeast": {
							"lat": 38.42976297989272,
							"lng": -121.4475693201073
						},
						"southwest": {
							"lat": 38.42706332010728,
							"lng": -121.4502689798927
						}
					}
				},
				"name": "Marion Mix Elementary School",
				"place_id": "ChIJjaDXTyTGmoARJ_tc3M_wvU0"
			}
		],
		"status": "OK"
	}
		`
	placeKeyError = `{
		"candidates": [],
		"error_message": "You must use an API key to authenticate each request to Google Maps Platform APIs. For additional information, please refer to http://g.co/dev/maps-no-account",
		"status": "REQUEST_DENIED"
	}`
)

func TestOkPlaceSearch(t *testing.T) {
	h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		keys := r.URL.Query()
		if keys.Get("input") != "Marion Mix Elementary" || keys.Get("location") != "38.428634643554688,-121.448753356933590" {
			t.Errorf("PlaceSearch API test failed\nExpected input: Marion Mix Elementary, got: %q\nExpected location: 38.428634643554688,-121.448753356933590, got: %s", keys.Get("input"), keys.Get("location"))
		}
		w.Write([]byte(placeOkResponse))
	})
	httpClient, teardown := testingHTTPClient(h)
	defer teardown()

	req := PlaceSearchRequest{
		Name: "Marion Mix Elementary",
		Lat:  "38.428634643554688",
		Lng:  "-121.448753356933590",
	}

	c, _ := NewClient("key", "")

	c.httpClient = httpClient

	resp, err := c.PlaceSearch(&req)
	if err != nil {
		t.Errorf("PlaceSearch API test failed: %q", err)
	}
	if resp.Candidates[0].Name != "Marion Mix Elementary School" || resp.Candidates[0].PlaceID != "ChIJjaDXTyTGmoARJ_tc3M_wvU0" {
		t.Errorf("Proposal API test failed\nParsed data mismatch.")
	}
}
func TestKeyErrorPlaceSearch(t *testing.T) {
	h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(placeKeyError))
	})
	httpClient, teardown := testingHTTPClient(h)
	defer teardown()

	req := PlaceSearchRequest{
		Name: "Marion Mix Elementary",
		Lat:  "38.428634643554688",
		Lng:  "-121.448753356933590",
	}

	c, _ := NewClient("key", "")

	c.httpClient = httpClient

	_, err := c.PlaceSearch(&req)
	if err == nil {
		t.Errorf("PlaceSearch API test (ErrorKey) failed: %q", err)
	}
}
