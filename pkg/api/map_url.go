package api

import (
	"fmt"
	"net/url"
	"time"

	"github.com/patrickmn/go-cache"
	log "github.com/sirupsen/logrus"
)

var mapURLsCache = cache.New(5*time.Minute, 10*time.Minute)

// GetMapURL creates a proper link to google maps
func (p *Proposal) GetMapURL(c *Client) string {
	if p.MapURL != "" {
		return p.MapURL
	}
	if cachedURL, found := mapURLsCache.Get(p.SchoolName + p.Latitude[0:3] + p.Longitude[0:3]); found {
		p.MapURL = cachedURL.(string)
		log.Info("Retrieving location data for ", p.SchoolName, " from cache")
		return p.MapURL
	}
	r := PlaceSearchRequest{Name: p.SchoolName, Lat: p.Latitude, Lng: p.Longitude, City: p.City}
	place, err := c.PlaceSearch(&r)
	if err != nil {
		log.Warn(err)
		return ""
	}
	p.MapURL = fmt.Sprintf("https://www.google.com/maps/search/?api=1&query=%s&query_place_id=%s", url.PathEscape(place.Candidates[0].Name), place.Candidates[0].PlaceID)
	mapURLsCache.Set(p.SchoolName+p.Latitude[0:3]+p.Longitude[0:3], p.MapURL, cache.NoExpiration)
	return p.MapURL
}
