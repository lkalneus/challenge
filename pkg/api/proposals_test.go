package api

import (
	"net/http"
	"testing"
)

const (
	okResponse = `{
		"searchTerms": "&#034;95758&#034;",
		"searchURL": "https://www.donorschoose.org/donors/search.html?zip=95758&keywords=%2295758%22&utm_source=api&utm_medium=feed&utm_content=searchlink&utm_campaign=ef4uju946azk",
		"totalProposals": "5",
		"index": "0",
		"max": "2",
		"breadcrumb": [
		  [
			"keywords",
			"&#034;95758&#034;",
			"Elk Grove, CA (95758) &#034;&#034;"
		  ],
		  [
			"zip",
			"95758",
			""
		  ],
		  [
			"max",
			"2",
			""
		  ]
		],
		"latitude": 38.432674407958984,
		"longitude": -121.43943023681641,
		"proposals": [
		  {
			"id": "4126207",
			"proposalURL": "https://www.donorschoose.org/project/interactive-learning-and-library-expansi/4126207/?utm_source=api&utm_medium=feed&utm_content=bodylink&utm_campaign=ef4uju946azk",
			"fundURL": "https://secure.donorschoose.org/donors/givingCart.html?proposalid=4126207&donationAmount=45&utm_source=api&utm_medium=feed&utm_content=fundlink&utm_campaign=ef4uju946azk",
			"imageURL": "https://www.donorschoose.org/teacher/photo/u5852564?size=sm&t=1536538815986",
			"retinaImageURL": "https://www.donorschoose.org/teacher/photo/u5852564?size=retina&t=1536538815986",
			"thumbImageURL": "https://www.donorschoose.org/teacher/photo/u5852564?size=thmb&t=1536538815986",
			"title": "Interactive Learning and Library Expansion",
			"shortDescription": "My students come from all socio-economic, ethnic, and family backgrounds but all have one thing in common- they need a little more help with their academics or behavior than some others at our...",
			"fulfillmentTrailer": "Help me give my students more interactive learning with Tower Math Games, CVC puzzles, and giant writing cubes and enjoyable love for reading with a bigger independent reading library with the leveled book library.",
			"snippets": [],
			"percentFunded": "0",
			"numDonors": "0",
			"costToComplete": "696.99",
			"studentLed": false,
			"numStudents": "28",
			"professionalDevelopment": false,
			"matchingFund": {
			  "matchingKey": "",
			  "ownerRegion": "",
			  "name": "",
			  "donorSalutation": "",
			  "type": "",
			  "matchImpactMultiple": "",
			  "multipleForDisplay": "",
			  "logoURL": "",
			  "faqURL": "",
			  "amount": "0.00",
			  "description": ""
			},
			"teacherFFPromoCodeFund": {
			  "eligible": "true",
			  "deadline": "May 22",
			  "code": "LIFTOFF",
			  "matchingKey": "LIFTOFF19",
			  "ownerRegion": "",
			  "name": "New Teacher Fund",
			  "donorSalutation": "New Teacher Fund",
			  "type": "PROMO",
			  "logoURL": "liftoff.gif",
			  "description": ""
			},
			"totalPrice": "696.99",
			"freeShipping": "true",
			"teacherId": "5852564",
			"teacherName": "Mrs. M.",
			"gradeLevel": {
			  "id": "2",
			  "name": "Grades 3-5"
			},
			"povertyLevel": "More than half of students from low‑income households",
			"povertyType": {
			  "label": "MODERATEHIGH",
			  "name": "More than half of students from low‑income households",
			  "range": "51-75%",
			  "showPovertyLevel": "true"
			},
			"teacherTypes": [],
			"schoolTypes": [],
			"schoolName": "Marion Mix Elementary",
			"schoolUrl": "https://www.donorschoose.org/school/marion-mix-elementary-opening-fall-2015/108314/",
			"city": "Elk Grove",
			"zip": "95758-5162",
			"state": "CA",
			"stateFullName": "California",
			"latitude": "38.428634643554688",
			"longitude": "-121.448753356933590",
			"zone": {
			  "id": "402",
			  "name": "California (North)"
			},
			"subject": {
			  "id": "3",
			  "name": "Literature &amp; Writing",
			  "groupId": "6"
			},
			"additionalSubjects": [{
			  "id": "23",
			  "name": "Special Needs",
			  "groupId": "7"
			}],
			"resource": {
			  "id": "15",
			  "name": "Educational Kits &amp; Games"
			},
			"expirationDate": "2019-07-09",
			"expirationTime": 1562644800000,
			"fundingStatus": "needs funding"
		  },
		  {
			"id": "3977885",
			"proposalURL": "https://www.donorschoose.org/project/interaction-simplified/3977885/?utm_source=api&utm_medium=feed&utm_content=bodylink&utm_campaign=ef4uju946azk",
			"fundURL": "https://secure.donorschoose.org/donors/givingCart.html?proposalid=3977885&donationAmount=45&utm_source=api&utm_medium=feed&utm_content=fundlink&utm_campaign=ef4uju946azk",
			"imageURL": "https://www.donorschoose.org/teacher/photo/u627142?size=sm&t=1475170470530",
			"retinaImageURL": "https://www.donorschoose.org/teacher/photo/u627142?size=retina&t=1475170470530",
			"thumbImageURL": "https://www.donorschoose.org/teacher/photo/u627142?size=thmb&t=1475170470530",
			"title": "Interaction Simplified",
			"shortDescription": "My classroom is for students who are ages 18-22 who are in the special education program; Adult Transition Program (ATP). In this class, we work on levels of independence with daily living skills...",
			"fulfillmentTrailer": "Help me give my students more unique access to the curriculum by providing us with a MimioTeach Interactive Whiteboard.  This is an amazing adaptation for my students who are in wheelchairs.",
			"snippets": [],
			"percentFunded": "43",
			"numDonors": "9",
			"costToComplete": "785.34",
			"studentLed": false,
			"numStudents": "13",
			"professionalDevelopment": false,
			"matchingFund": {
			  "matchingKey": "",
			  "ownerRegion": "",
			  "name": "",
			  "donorSalutation": "",
			  "type": "",
			  "matchImpactMultiple": "",
			  "multipleForDisplay": "",
			  "logoURL": "",
			  "faqURL": "",
			  "amount": "0.00",
			  "description": ""
			},
			"totalPrice": "1379.16",
			"freeShipping": "false",
			"teacherId": "627142",
			"teacherName": "Mrs. D.",
			"gradeLevel": {
			  "id": "4",
			  "name": "Grades 9-12"
			},
			"povertyLevel": "More than half of students from low‑income households",
			"povertyType": {
			  "label": "MODERATEHIGH",
			  "name": "More than half of students from low‑income households",
			  "range": "51-75%",
			  "showPovertyLevel": "true"
			},
			"teacherTypes": [
			  {
				"id": 9,
				"name": "NEA member"
			  },
			  {
				"id": 5,
				"name": "SONIC Teacher"
			  }
			],
			"schoolTypes": [],
			"schoolName": "Laguna Creek High School",
			"schoolUrl": "https://www.donorschoose.org/school/laguna-creek-high-school/61211/",
			"city": "Elk Grove",
			"zip": "95758-5859",
			"state": "CA",
			"stateFullName": "California",
			"latitude": "38.428601000000000",
			"longitude": "-121.427322000000000",
			"zone": {
			  "id": "402",
			  "name": "California (North)"
			},
			"subject": {
			  "id": "16",
			  "name": "College &amp; Career Prep",
			  "groupId": "5"
			},
			"additionalSubjects": [{
			  "id": "23",
			  "name": "Special Needs",
			  "groupId": "7"
			}],
			"resource": {
			  "id": "10",
			  "name": "Classroom Basics"
			},
			"expirationDate": "2019-06-27",
			"expirationTime": 1561608000000,
			"fundingStatus": "needs funding"
		  }
		]
	  }`
)

func TestOkProposalSearch(t *testing.T) {
	h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		keys := r.URL.Query()
		if keys.Get("zip") != "95758" {
			t.Errorf("Proposal api test failed\nExpected zip code: 95758, got: %q", keys.Get("zip"))
		}
		w.Write([]byte(okResponse))
	})
	httpClient, teardown := testingHTTPClient(h)
	defer teardown()

	req := ProposalsSearchRequest{
		Zip:   "95758",
		Max:   2,
		Index: 0,
	}

	c, _ := NewClient("key", "")

	c.httpClient = httpClient

	resp, err := c.ProposalsSearch(&req)
	if err != nil {
		t.Errorf("Proposal API test failed: %q", err)
	}
	if resp.TotalProposals != "5" || len(resp.Proposals) != 2 || resp.Index != "0" {
		t.Errorf("Proposal API test failed\nTotal proposals: %s, expected: 5\nProposals received: %d, expected: 2\nIndex: %s, expected 0\n", resp.TotalProposals, len(resp.Proposals), resp.Index)
	}
}

func TestAPIFailProposalSearch(t *testing.T) {
	h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("500 - Something bad happened!"))
	})
	httpClient, teardown := testingHTTPClient(h)
	defer teardown()

	req := ProposalsSearchRequest{
		Zip:   "95758",
		Max:   2,
		Index: 0,
	}

	c, _ := NewClient("key", "")

	c.httpClient = httpClient

	_, err := c.ProposalsSearch(&req)

	if err.Error() != proposalsBadResponseMsg {
		t.Errorf("Proposal API test failed\nExpected bad response from API\n%q", err)
	}
}
