package api

import (
	"errors"
	"net/url"

	log "github.com/sirupsen/logrus"
)

var placeSearchAPI = &apiConfig{
	host: "https://maps.googleapis.com",
	path: "maps/api/place/findplacefromtext/json",
}

// PlaceSearchResponse describes search response
type PlaceSearchResponse struct {
	Candidates []struct {
		Geometry struct {
			Location struct {
				Lat float64 `json:"lat"`
				Lng float64 `json:"lng"`
			} `json:"location"`
		} `json:"geometry"`
		Name    string `json:"name"`
		PlaceID string `json:"place_id"`
	} `json:"candidates,omitempty"`
	Status string `json:"status"`
}

func (p *PlaceSearchResponse) valid() bool {
	if p.Status != "OK" {
		log.Warn("Can't generate map URL. Error: API status is - ", p.Status)
		return false
	}
	if len(p.Candidates) == 0 {
		log.Warn("Can't generate map URL. Error: place wasn't found")
		return false
	}
	return true
}

// PlaceSearchRequest describes search params.
type PlaceSearchRequest struct {
	// School name
	Name string
	// School city
	City string
	// School latitude
	Lat string
	// School longtitude
	Lng string
}

func (r *PlaceSearchRequest) valid() bool {
	if r.Name == "" && r.Lat == "" && r.Lng == "" {
		log.Warn("Could not generate map URL. Error: Bad request")
		return false
	}
	return true
}

func (r *PlaceSearchRequest) params() url.Values {

	q := make(url.Values)

	if r.City != "" {
		q.Set("input", r.Name+" "+r.City)
	} else {
		q.Set("input", r.Name)
	}
	q.Set("inputtype", "textquery")
	q.Set("fields", "name,geometry/location,place_id")
	q.Set("location", r.Lat+","+r.Lng)

	return q
}

// PlaceSearch lets you search for places within a specified area.
func (c *Client) PlaceSearch(r *PlaceSearchRequest) (PlaceSearchResponse, error) {

	if !r.valid() {
		return PlaceSearchResponse{}, errors.New("Could not generate map URL. Error: Bad request")
	}

	var response PlaceSearchResponse

	if err := c.getJSON(placeSearchAPI, r, &response); err != nil {
		return PlaceSearchResponse{}, err
	}

	if !response.valid() {
		log.Warn(r.params())
		return PlaceSearchResponse{}, errors.New("Could not generate map URL. Error: Bad response")
	}

	return response, nil
}
