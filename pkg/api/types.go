package api

//Proposal describes project's structure.
type Proposal struct {
	ID                      string `json:"id"`
	ProposalURL             string `json:"proposalURL"`
	MapURL                  string
	Title                   string `json:"title"`
	ShortDescription        string `json:"shortDescription"`
	FulfillmentTrailer      string `json:"fulfillmentTrailer"`
	PercentFunded           string `json:"percentFunded"`
	NumDonors               string `json:"numDonors"`
	CostToComplete          string `json:"costToComplete"`
	StudentLed              bool   `json:"studentLed"`
	NumStudents             string `json:"numStudents"`
	ProfessionalDevelopment bool   `json:"professionalDevelopment"`
	TotalPrice              string `json:"totalPrice"`
	TeacherID               string `json:"teacherId"`
	TeacherName             string `json:"teacherName"`
	SchoolName              string `json:"schoolName"`
	SchoolURL               string `json:"schoolUrl"`
	City                    string `json:"city"`
	Zip                     string `json:"zip"`
	State                   string `json:"state"`
	StateFullName           string `json:"stateFullName"`
	Latitude                string `json:"latitude"`
	Longitude               string `json:"longitude"`
	Subject                 struct {
		ID      string `json:"id"`
		Name    string `json:"name"`
		GroupID string `json:"groupId"`
	} `json:"subject"`
	AdditionalSubjects []struct {
		ID      string `json:"id"`
		Name    string `json:"name"`
		GroupID string `json:"groupId"`
	} `json:"additionalSubjects,omitempty"`
	Resource struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"resource"`
	ExpirationDate string `json:"expirationDate"`
	ExpirationTime int64  `json:"expirationTime"`
	FundingStatus  string `json:"fundingStatus"`
}
