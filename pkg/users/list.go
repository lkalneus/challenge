package users

import (
	"fmt"

	"bitbucket.org/lkalneus/promptui"
	log "github.com/sirupsen/logrus"
)

//TheUser - current user
func TheUser() User {
	return theUser
}

var theUser User

//ListUsersState shows users select menu
func ListUsersState() string {
	var state string
	var items []string
	items = append(items, "New user")
	ulist, err := getUsers()
	for _, i := range ulist {
		items = append(items, i.Name+" ("+i.Email+")")
	}
	if err != nil {
		log.Warn(err)
	}

	prompt := promptui.Select{
		Label: "Select an existing user or create new",
		Items: items,
	}

	i, result, err := prompt.Run()

	if err != nil {
		fmt.Printf("Exit %v\n", err)
		return state
	}
	if result == "New user" {
		return "CreateUserState"
	}
	theUser = ulist[i-1]
	fmt.Printf("Welcome %s!\n", theUser.Name)
	return "ShowProjectsState"

}
