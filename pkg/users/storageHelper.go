package users

import (
	"encoding/json"
	"errors"
	"strings"

	db "bitbucket.org/lkalneus/challenge/pkg/storage"
)

// ToDo email should be encrypted
func saveUser(u User) error {
	data, err := json.Marshal(u)
	if err != nil {
		return errors.New("Could not serialize user data")
	}
	return db.Save(u.Name+u.Email, string(data))
}
func getUser(name, email string) (u User, err error) {
	val, err := db.Get(name + email)
	if err != nil {
		return u, errors.New("Could not load user data")
	}
	err = json.Unmarshal([]byte(val), &u)
	if err != nil {
		return u, errors.New("Could not load user data")
	}
	return
}
func delUser(name, email string) error {
	return db.Del(name + email)
}
func getUsers() (users []User, err error) {
	values := strings.Split(db.GetAll(), "|±|")

	for _, v := range values {
		if v == "" {
			continue
		}
		var u User
		err = json.Unmarshal([]byte(v), &u)
		users = append(users, u)
		if err != nil {
			return users, errors.New("Could not load users data")
		}

	}
	return
}
