package users

import (
	"testing"

	db "bitbucket.org/lkalneus/challenge/pkg/storage"

	"github.com/mitchellh/go-homedir"
)

func TestZipcodeValidation(t *testing.T) {
	if isZipcodeValid("95035") != nil {
		t.Errorf("Zipcode validation does not work correctly")
	}
}

func TestStorageHelper(t *testing.T) {
	dbPath, _ := homedir.Dir()
	err := db.InitStorage(dbPath)
	if err != nil {
		t.Errorf("Could not initialize storage. Got: %q", err)
	}
	u := User{Name: "Username", Email: "Email", Zipcode: "99999"}
	err = saveUser(u)
	if err != nil {
		t.Errorf("Could not save user. Got: %q", err)
	}

	u1, err := getUser(u.Name, u.Email)
	if err != nil {
		t.Errorf("Could not get user data. Got: %q", err)
	}
	if u1.Zipcode != "99999" {
		t.Errorf("Incorrect user data. Expected: %s. Got: %s", "99999", u1.Zipcode)
	}
	u = User{Name: "Username1", Email: "Email1", Zipcode: "999991"}
	err = saveUser(u)
	if err != nil {
		t.Errorf("Could not save user. Got: %q", err)
	}
	list, err := getUsers()
	if len(list) < 2 || err != nil {
		t.Errorf("Could not get data for all users")
	}

	delUser("Username", "Email")
	delUser("Username1", "Email1")
}
