package users

import (
	"errors"
	"fmt"
	"regexp"

	"bitbucket.org/lkalneus/promptui"
)

// User describes user properties
type User struct {
	Name    string `json:"name"`
	Email   string `json:"email"`
	Zipcode string `json:"zipcode"` //  RegExp: /(^\d{5}$)|(^\d{5}-\d{4}$)/
}

func isNameValid(name string) (err error) {
	if len(name) == 0 {
		return errors.New("Username cannot be empty")
	}
	return
}

func isEmailValid(email string) (err error) {
	// RegExp was taken from https://github.com/asaskevich/govalidator/blob/master/patterns.go
	re := regexp.MustCompile("^(((([a-zA-Z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])+(\\.([a-zA-Z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])|(\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-zA-Z]|\\d|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])|(([a-zA-Z]|\\d|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])([a-zA-Z]|\\d|-|\\.|_|~|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])*([a-zA-Z]|\\d|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])))\\.)+(([a-zA-Z]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])|(([a-zA-Z]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])([a-zA-Z]|\\d|-|_|~|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])*([a-zA-Z]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])))\\.?$")
	v := re.MatchString(email)
	if !v {
		err = errors.New("Invalid email")
	}
	return
}

// isZipcodeValid checks that user provided valid zip code for US
func isZipcodeValid(zipcode string) (err error) {
	const r = `^[0-9]{5}(?:-[0-9]{4})?$`
	v, _ := regexp.MatchString(r, zipcode)
	if !v {
		err = errors.New("Invalid zip code (US codes only)")
	}
	return
}

// CreateUserState gets and validates username and zipcode from user.
func CreateUserState() string {
	var state string
	var err error
	var u User
	prompt := promptui.Prompt{
		Label:    "Username: ",
		Validate: isNameValid,
	}

	u.Name, err = prompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return state
	}
	prompt = promptui.Prompt{
		Label:    "Email: ",
		Validate: isEmailValid,
	}

	u.Email, err = prompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return state
	}

	prompt = promptui.Prompt{
		Label:    "Zip code: ",
		Validate: isZipcodeValid,
	}

	u.Zipcode, err = prompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return state
	}
	err = saveUser(u)
	if err != nil {
		fmt.Printf("Could not create user\n%q", err)
		return state
	}
	fmt.Println(u.Name, " added!")
	return "ListUsersState"
}
