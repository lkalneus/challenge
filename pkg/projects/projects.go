//Package projects gets & display the data from APIs
package projects

import (
	"errors"
	"fmt"
	"html"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"bitbucket.org/lkalneus/promptui"

	api "bitbucket.org/lkalneus/challenge/pkg/api"
	user "bitbucket.org/lkalneus/challenge/pkg/users"

	log "github.com/sirupsen/logrus"
)

//MapsAPIKey is a key for Google Places API
var MapsAPIKey = ""

//DonorsChooseAPIKey is a key for Donors Choose API
var DonorsChooseAPIKey = ""

//NotStrictMode is a flag for not strict search mode. If it's true - you will get plenty of results in a larger area
var NotStrictMode = false

//ProjectsPerScreen is a flag to control number of items per screen. Default is ten.
var ProjectsPerScreen = 10

func getProjects(zip string, max, index int, cDonorsChoose, cMaps *api.Client) ([]api.Proposal, int, error) {
	req := api.ProposalsSearchRequest{Zip: user.TheUser().Zipcode, Max: max, Index: index, NotStrict: NotStrictMode}

	var projects []api.Proposal
	log.Info("Making the projects request. Zip code: ", zip, " Max: ", max, " Index: ", index)
	res, err := cDonorsChoose.ProposalsSearch(&req)
	if err != nil {
		log.Warn("Could not get project list. ", err)
		return projects, 0, err
	}

	totalCount, err := strconv.Atoi(res.TotalProposals)
	log.Info("Total count: ", totalCount)
	if totalCount == 0 {
		log.Warn("Empty project list")
		return projects, totalCount, errors.New("No projects found. Zip code: " + zip)
	}

	projects = append(projects, res.Proposals...)
	for i := range projects {
		projects[i].Title = html.UnescapeString(projects[i].Title)
		projects[i].TeacherName = html.UnescapeString(projects[i].TeacherName)
		projects[i].FulfillmentTrailer = html.UnescapeString(projects[i].FulfillmentTrailer)
		projects[i].SchoolName = html.UnescapeString(projects[i].SchoolName)
		log.Info("Looking up location data for ", projects[i].SchoolName)
		projects[i].GetMapURL(cMaps)
	}

	return projects, totalCount, err
}

func getAllProjects() (projects []api.Proposal, err error) {

	cDonorsChoose, _ := api.NewClient(DonorsChooseAPIKey, "")
	cMaps, _ := api.NewClient(MapsAPIKey, "")

	//max per request = 50 - API limit
	index := 0
	_, total, err := getProjects(user.TheUser().Zipcode, 1, index, cDonorsChoose, cMaps)
	if err != nil {
		log.Warn("!", err)
		return projects, err
	}
	// naming step - is just because. there is no much sense in it.
	var step int = total/50 + 50
	log.Info("Step: ", step)
	var wg sync.WaitGroup
	var wg2 sync.WaitGroup
	ch := make(chan []api.Proposal, 1050)
	for index = 0; index < step && index < 1001 && step != 0; index = index + 50 {
		log.Info("Index is ", index)
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			newProjects, _, err := getProjects(user.TheUser().Zipcode, 50, i, cDonorsChoose, cMaps)
			log.Info("Got the results for index: ", i, "; Length: ", len(newProjects))
			if err != nil {
				log.Warn(err)
			}
			ch <- newProjects
		}(index)

	}
	wg2.Add(1)
	go func() {
		defer wg2.Done()
		for p := range ch {
			projects = append(projects, p...)
			log.Info("Okay, new length: ", len(projects))
		}
	}()

	wg.Wait()
	close(ch)
	log.Info("#TIME:", time.Now())
	wg2.Wait()
	return projects, err
}

//ShowProjectsState displays list of projects for current user
func ShowProjectsState() string {
	var state string
	fmt.Println("Fetching projects in your area. We won't be too long – thanks for your patience!")
	timeStart := time.Now()
	projects, err := getAllProjects()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println("Loaded", len(projects), "projects in", time.Since(timeStart).Truncate(time.Millisecond))
	if len(projects) == 0 {
		return state
	}
	templates := &promptui.SelectTemplates{
		Label:    "{{ `--------- Projects list ---------------`  }}",
		Active:   "\U0001F98A {{ .Title | cyan }} ({{ .PercentFunded | red }}{{ \"% funded\" | red }} - ${{ .CostToComplete }} still required)",
		Inactive: "  {{ .Title | cyan }} ({{ .PercentFunded | red }}{{ \"% funded\" | red }})",
		Selected: "\U0001F98A {{ .Title | red | cyan }} - {{ .ProposalURL }}",
		Details: `
	--------- {{ .Title }} ----------
	{{ "Location:" | faint }}	{{.Zip}}, {{ .City }}, {{ .State }} ({{ .MapURL }})
	{{ "School:" | faint }}	{{ .SchoolName }}
	{{ "Teacher:" | faint }}	{{ .TeacherName }}
	{{ "Description:" | faint }}	{{ .FulfillmentTrailer }}
	`,
	}

	searcher := func(input string, index int) bool {
		prop := projects[index]
		name := strings.Replace(strings.ToLower(prop.Title), " ", "", -1)
		school := strings.Replace(strings.ToLower(prop.SchoolName), " ", "", -1)
		teacher := strings.Replace(strings.ToLower(prop.TeacherName), " ", "", -1)
		input = strings.Replace(strings.ToLower(input), " ", "", -1)

		return strings.Contains(name, input) || strings.Contains(school, input) || strings.Contains(teacher, input)
	}

	prompt := promptui.Select{
		Label:     "",
		Items:     projects,
		Templates: templates,
		Size:      ProjectsPerScreen,
		Searcher:  searcher,
	}

	_, _, err = prompt.Run()

	if err != nil {
		fmt.Printf("Exit %v\n", err)
		return state
	}

	return state
}
