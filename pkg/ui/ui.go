// Package ui provides a simple states manager
package ui

import (
	"os"

	projects "bitbucket.org/lkalneus/challenge/pkg/projects"
	users "bitbucket.org/lkalneus/challenge/pkg/users"
)

// Show is a simple states manager
// There are 4 states:
// CreateUserState - display new user prompt
// ListUsersState - display users list menu
// ShowProjectsState - display list of projects
// default - exit the app
func Show(state string) {
	switch state {
	case "CreateUserState":
		Show(users.CreateUserState())
	case "ListUsersState":
		Show(users.ListUsersState())
	case "ShowProjectsState":
		Show(projects.ShowProjectsState())

	default:
		os.Exit(0)
	}
}
