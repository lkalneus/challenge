//Package storage provides a boltdb storage for users and simple operations with them
package storage

import (
	"errors"

	"github.com/boltdb/bolt"
	log "github.com/sirupsen/logrus"
)

var storagePath string

var bucketUsers = []byte("users")

// InitStorage creates/opens storage for users
func InitStorage(path string) error {
	// Open the bckChallenge.db data file in current directory
	// It will be created if it doesn't exist
	storagePath = path + "/.bckChallenge.db"
	db, err := bolt.Open(storagePath, 0600, nil)
	if err != nil {
		return err
	}
	// Create the bucket for users, if it doesn't exist
	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(bucketUsers)
		if err != nil {
			return err
		}
		return nil
	})
	defer db.Close()
	return err
}

// Save stores a key-value pair in the users bucket
func Save(key, value string) error {
	db, err := bolt.Open(storagePath, 0600, nil)

	if err != nil {
		return errors.New("Storage error")
	}
	defer db.Close()
	err = db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(bucketUsers)
		err = bucket.Put([]byte(key), []byte(value))
		if err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}

	return nil
}

// Get retrieves a single value from the users bucket
func Get(key string) (string, error) {
	var val string

	db, err := bolt.Open(storagePath, 0600, nil)

	if err != nil {
		return val, errors.New("Storage error")
	}

	defer db.Close()

	err = db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(bucketUsers)
		if bucket == nil {
			return errors.New("Storage error: bucket not found")
		}
		val = string(bucket.Get([]byte(key)))

		return nil
	})
	return val, err
}

// GetAll reads all the values from the users bucket
func GetAll() string {
	var values string

	db, err := bolt.Open(storagePath, 0600, nil)

	if err != nil {
		log.Warn(err)
		return ""
	}
	defer db.Close()

	err = db.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		bucket := tx.Bucket(bucketUsers)

		err := bucket.ForEach(func(k, v []byte) error {
			values = values + "|±|" + string(v)

			return nil
		})
		return err
	})
	if err != nil {
		log.Warn(err)
	}
	return values
}

// Removes single key-value pair from storage
func Del(key string) error {

	db, err := bolt.Open(storagePath, 0600, nil)

	if err != nil {
		return errors.New("Storage error")
	}

	defer db.Close()

	err = db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(bucketUsers)

		err = bucket.Delete([]byte(key))

		if err != nil {
			return err
		}
		return nil
	})
	return nil
}
