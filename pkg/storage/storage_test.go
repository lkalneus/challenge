package storage

import (
	"testing"

	homedir "github.com/mitchellh/go-homedir"
)

// TestStorage tests all basic storage functions (Init, Save, Get, Del)
func TestStorage(t *testing.T) {
	dbPath, _ := homedir.Dir()
	err := InitStorage(dbPath)
	if err != nil {
		t.Errorf("Could not initialize storage. Got: %q", err)
	}
	err = Save("test", "testVal")
	if err != nil {
		t.Errorf("Saving to storage does not work correctly")
	}
	val, err := Get("test")
	if err != nil {
		t.Errorf("Reading from storage does not work correctly")
	}

	if string(val) != "testVal" {
		t.Errorf("Reading from storage does not work correctly\nExpected: testVal, Got: %s", string(val))
	}

	err = Del("test")
	if err != nil {
		t.Errorf("Deleting from storage does not work correctly")
	}
}
