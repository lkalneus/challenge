// Copyright © 2019 Leonid Kalneus <l.kalneus@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"errors"
	"fmt"
	"os"

	"bitbucket.org/lkalneus/challenge/pkg/projects"
	db "bitbucket.org/lkalneus/challenge/pkg/storage"
	"bitbucket.org/lkalneus/challenge/pkg/ui"

	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func init() {
	pflag.ErrHelp = errors.New("")
	pflag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s --googlePlacesKey <key> --donorsChooseKey <key> [...]\n\n", os.Args[0])
		pflag.PrintDefaults()
	}
	pflag.String("googlePlacesKey", "", "Google Places API Key")
	pflag.String("donorsChooseKey", "", "DonorsChoose API Key")
	pflag.StringP("homeDir", "d", "", "Path for users storage. Default is your home directory")
	pflag.Int("perPage", 10, "Items per screen")
	pflag.Bool("nonStrict", false, "Enables not strict search")
	pflag.BoolP("verbose", "v", false, "Show log messages")
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)

	projects.NotStrictMode = viper.GetBool("not-strict")
	projects.ProjectsPerScreen = viper.GetInt("perPage")
	if projects.ProjectsPerScreen < 1 {
		projects.ProjectsPerScreen = 10
	}

	log.SetLevel(log.FatalLevel)
	if viper.GetBool("verbose") {
		log.SetLevel(log.InfoLevel)
	}

	// API Keys
	projects.MapsAPIKey = viper.GetString("googlePlacesKey")
	if projects.MapsAPIKey == "" {
		fmt.Println("googlePlacesKey is required")
		pflag.Usage()
		os.Exit(1)
	}
	projects.DonorsChooseAPIKey = viper.GetString("donorsChooseKey")
	if projects.DonorsChooseAPIKey == "" {
		fmt.Println("donorsChooseKey is required")
		pflag.Usage()
		os.Exit(1)
	}

	// Storage init
	dbPath := viper.GetString("homedir")
	if dbPath == "" {
		var err error
		dbPath, err = homedir.Dir()
		if err != nil {
			fmt.Println("Specify location for users storage")
			os.Exit(1)
		}
	}
	log.Info("dbPath:", dbPath)
	err := db.InitStorage(dbPath)
	if err != nil {
		log.Warn("Unable to intialize the storage; Error:", err)
		fmt.Println("Unable to intialize the storage")
		os.Exit(1)
	}
}

func main() {
	ui.Show("ListUsersState")
}
